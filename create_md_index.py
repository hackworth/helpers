#!/usr/bin/env python3

import argparse
from pathlib import Path
import time

MARKER = "## Generated Index\n"

def write_index_file(index_file, file_list, starting_path, delete_backup):
	"""Writes an index file, making sure to append the auto generated part to the bottom of the file."""

	line = None

	if index_file.exists():
		backupfile = index_file.with_suffix(".bak.{}".format(int(time.time())))
		index_file.rename(backupfile)
		with index_file.open('w') as outfile:
			with backupfile.open() as infile:
				for line in infile.readlines():
					if line == MARKER:
						break
					else:
						outfile.write(line)
				if line is not None and not line.endswith('\n'):
					outfile.write("\n")
		if delete_backup:
			backupfile.unlink()

	with index_file.open('a') as outfile:
		outfile.write(MARKER)
		outfile.write("\n_Created {}_\n\n".format(time.strftime('%a, %d %b %Y %H:%M:%S +0000', time.gmtime())))
		for md_file in file_list:
			linkname = md_file.relative_to(starting_path)
			linkname = str(linkname).rsplit('.',1)[0]
			link = linkname.replace(' ', '%20')
			outfile.write(f" * [{linkname}]({link})\n")

def main():
	parser = argparse.ArgumentParser(description='build an index file for the ',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument('--indexfile', default='index.md', help='Name for the index file.')
	parser.add_argument('--delete_backup', action='store_true', help='Keep a backup of the old index file.')
	parser.add_argument('wiki_repo_path', default='.', nargs='?', help='the directory containg the wiki files.')
	args = parser.parse_args()
	
	starting_path = Path(args.wiki_repo_path).resolve()
	indexfile = starting_path / args.indexfile
	md_files = list(starting_path.glob('**/*.md'))
	md_files.sort()
	write_index_file(indexfile, md_files, starting_path, args.delete_backup)

if __name__ == '__main__':
	main()

